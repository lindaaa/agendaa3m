package com.example.agendaescolar.services;

import android.content.Context;
import android.os.AsyncTask;
import android.widget.EditText;

import com.example.agendaescolar.CadastroDeUsuarioActivity;
import com.example.agendaescolar.R;

import org.json.JSONException;
import org.json.JSONObject;
import java.lang.ref.WeakReference;

public class ConsultaCepTask  extends AsyncTask<String, Object, String> {

    private WeakReference<CadastroDeUsuarioActivity> reference;

    public ConsultaCepTask(Context context) {
        this.reference = new WeakReference<>(
                (CadastroDeUsuarioActivity) context);
    }

    @Override
    protected String doInBackground(String... listaCep) {
        String cep = listaCep[0];

        WebClient client = new WebClient();
        String response = client.getCep(cep);

        return response;
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);

        try {
            JSONObject json = new JSONObject(s);

            String rua = json.getString("logradouro");
            String cidade = json.getString("localidade");
            String uf = json.getString("uf");

            EditText editTextRua = reference.get().
                    findViewById(R.id.edtLogradouro);
            editTextRua.setText(rua);

            EditText editTextCidade = reference.get().findViewById(R.id.edtCidade);
            editTextCidade.setText(cidade);

            EditText edtUF = reference.get().findViewById(R.id.edtUF);
            edtUF.setText(uf);

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }
}
