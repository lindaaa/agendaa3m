package com.example.agendaescolar;

import android.app.Activity;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;

import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

public class notificacoes {

    String idDoCanal = "canal da notificacao 1";

    public void notificar(Context context, String titulo, String descricao){
        NotificationCompat.Builder criadorNotificacoes = new NotificationCompat.Builder(context, idDoCanal);
        criadorNotificacoes.setContentTitle(titulo)
                .setContentText(descricao)
                .setSmallIcon(R.drawable.logo)
                .setPriority(Notification.PRIORITY_HIGH);

        NotificationManagerCompat notManager = NotificationManagerCompat.from(context);
        notManager.notify(123, criadorNotificacoes.build());
    }

    public void criarCanalNotificacao(Activity activity){
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            NotificationChannel canal = new NotificationChannel(
                    idDoCanal,
                    "Notificacao do Evento",
                    NotificationManager.IMPORTANCE_DEFAULT
            );

            NotificationManager notManager = (NotificationManager) activity.getSystemService(Context.NOTIFICATION_SERVICE);
            notManager.createNotificationChannel(canal);
        }
    }
}
