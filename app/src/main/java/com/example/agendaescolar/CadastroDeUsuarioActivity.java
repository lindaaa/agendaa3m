package com.example.agendaescolar;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.agendaescolar.modelagem.Usuario;
import com.example.agendaescolar.modelagem.db.UsuariosDAO;
import com.example.agendaescolar.services.ConsultaCepTask;

import java.util.concurrent.ExecutionException;

public class CadastroDeUsuarioActivity extends AppCompatActivity {

    private ConsultaCepTask cepTask;

    private EditText edtNome;
    private EditText edtEmail;
    private EditText edtDataNascimento;
    private EditText edtUF;
    private EditText edtLogradouro;
    private EditText edtCidade;
    private EditText edtSenha;
    private EditText edtRepetirSenha;
    private EditText edtCEP;
    private Button btnBuscar;
    private Button cadastrarUsuario;

    private Intent minhaIntent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cadastro_de_usuario);

        cepTask = new ConsultaCepTask(this);

        edtNome = findViewById(R.id.edtNome);
        edtEmail = findViewById(R.id.edtEmail);
        edtDataNascimento = findViewById(R.id.edtDataNascimento);
        edtUF = findViewById(R.id.edtUF);
        edtLogradouro = findViewById(R.id.edtLogradouro);
        edtCidade = findViewById(R.id.edtCidade);
        edtSenha = findViewById(R.id.edtSenha);
        edtRepetirSenha = findViewById(R.id.edtRepetirSenha);
        edtCEP = findViewById(R.id.edtCEP);

        minhaIntent = getIntent();

        btnBuscar = findViewById(R.id.btnBuscar);
        btnBuscar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String cep = edtCEP.getText().toString();

                if(!cep.equals("")) {
                    cepTask.execute(cep);
                }
            }
        });

        cadastrarUsuario = findViewById(R.id.cadastrarUsuario);
        cadastrarUsuario.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String Nome = edtNome.getText().toString();
                String Email = edtEmail.getText().toString();
                String DataNascimento = edtDataNascimento.getText().toString();
                String cep = edtCEP.getText().toString();
                String UF = edtUF.getText().toString();
                String Logradouro = edtLogradouro.getText().toString();
                String Cidade = edtCidade.getText().toString();
                String Senha = edtSenha.getText().toString();
                String RepetirSenha = edtRepetirSenha.getText().toString();

                if(Senha.equals(RepetirSenha)) {


                    Usuario user = new Usuario(
                            0,
                            Nome,
                            Email,
                            Senha,
                            DataNascimento,
                            cep,
                            Logradouro,
                            Cidade,
                            UF,
                            ""
                    );

                    new UsuariosDAO(CadastroDeUsuarioActivity.this).insert(user);

                    finish();

                }
                else {
                    Toast.makeText(CadastroDeUsuarioActivity.this, "As senhas não são iguais", Toast.LENGTH_SHORT).show();
                }

            }
        });
    }
}
