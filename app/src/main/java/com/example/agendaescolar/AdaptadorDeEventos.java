package com.example.agendaescolar;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.agendaescolar.modelagem.Evento;

import java.util.ArrayList;

public class AdaptadorDeEventos extends BaseAdapter {

    private Context context;
    private ArrayList<Evento> eventos;

    public AdaptadorDeEventos(Context context, ArrayList<Evento> eventos) {
        this.context = context;
        this.eventos = eventos;
    }

    @Override
    public int getCount() {
        return eventos.size();
    }

    @Override
    public Object getItem(int position) {
        return eventos.get(position);
    }

    @Override
    public long getItemId(int position) {
        return eventos.get(position).getId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = LayoutInflater.from(this.context).inflate(R.layout.item_evento, null, false);

        TextView titulo = view.findViewById(R.id.titulo_do_evento);
        titulo.setText(this.eventos.get(position).getNome());

        TextView descricao = view.findViewById(R.id.descricao_do_evento);
        descricao.setText(this.eventos.get(position).getDescricao());

        TextView data = view.findViewById(R.id.data_do_evento);
        data.setText(this.eventos.get(position).getData());

        ImageView imagem = view.findViewById(R.id.img_do_evento);
//        Bitmap bm = BitmapFactory.decodeFile("");
//        imagem.setImageBitmap(bm);

        return view;
    }

    public void atualizar(ArrayList<Evento> eventos) {
        this.eventos = eventos;

        notifyDataSetChanged();
    }
}
