package com.example.agendaescolar;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.FileProvider;

import com.example.agendaescolar.modelagem.Usuario;
import com.example.agendaescolar.services.ConsultaCepTask;

import java.io.File;

public class VerDadosDoUsuarioActivity extends AppCompatActivity {

    private ConsultaCepTask cepTask;

    private EditText edtNome;
    private EditText edtEmail;
    private EditText edtDataNascimento;
    private EditText edtUF;
    private EditText edtLogradouro;
    private EditText edtCidade;
    private EditText edtSenha;
    private EditText edtRepetirSenha;
    private EditText edtCEP;
    private Button btnBuscar;
    private Button cadastrarUsuario;
    private ImageView imgFoto;

    private String caminhoDaFoto = "";

    private Intent minhaIntent;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ver_dados);

        cepTask = new ConsultaCepTask(this);

        edtNome = findViewById(R.id.edtNome);
        edtEmail = findViewById(R.id.edtEmail);
        edtDataNascimento = findViewById(R.id.edtDataNascimento);
        edtUF = findViewById(R.id.edtUF);
        edtLogradouro = findViewById(R.id.edtLogradouro);
        edtCidade = findViewById(R.id.edtCidade);
        edtSenha = findViewById(R.id.edtSenha);
        edtRepetirSenha = findViewById(R.id.edtRepetirSenha);
        edtCEP = findViewById(R.id.edtCEP);
        imgFoto = findViewById(R.id.imgFoto);

        minhaIntent = getIntent();

        Button btnPegarFoto = findViewById(R.id.btnPegarFoto);

        Usuario usuario = (Usuario) minhaIntent.getSerializableExtra("usuario");

        if(!usuario.getImg().equals("")) {
            Bitmap bm = BitmapFactory.decodeFile(usuario.getImg());

            imgFoto.setImageBitmap(bm);
        }

        btnPegarFoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

                caminhoDaFoto = getExternalFilesDir(null) + "/" + System.currentTimeMillis() + ".jpg";
                File file = new File(caminhoDaFoto);
                intent.putExtra(MediaStore.EXTRA_OUTPUT,
                        FileProvider.getUriForFile(
                                VerDadosDoUsuarioActivity.this,
                                BuildConfig.APPLICATION_ID + ".provider",
                                file
                        ));

                startActivityForResult(intent, 123);


            }
        });

        btnBuscar = findViewById(R.id.btnBuscar);
        btnBuscar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String cep = edtCEP.getText().toString();

                if(!cep.equals("")) {
                    cepTask.execute(cep);
                }
            }
        });

        cadastrarUsuario = findViewById(R.id.cadastrarUsuario);
        cadastrarUsuario.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String Nome = edtNome.getText().toString();
                String Email = edtEmail.getText().toString();
                String DataNascimento = edtDataNascimento.getText().toString();
                String UF = edtUF.getText().toString();
                String Logradouro = edtLogradouro.getText().toString();
                String Cidade = edtCidade.getText().toString();
                String Senha = edtSenha.getText().toString();
                String RepetirSenha = edtRepetirSenha.getText().toString();
                String foto = imgFoto.getTag().toString();

            }
        });
    }
}
