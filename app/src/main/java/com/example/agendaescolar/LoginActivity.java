package com.example.agendaescolar;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.agendaescolar.modelagem.Usuario;
import com.example.agendaescolar.modelagem.db.UsuariosDAO;

public class LoginActivity extends AppCompatActivity {

    private EditText edtEmail;
    private EditText edtSenha;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        edtEmail = findViewById(R.id.edtlogin);
        edtSenha = findViewById(R.id.edtsenha);

        Button buton = findViewById(R.id.btnEntrar);
        Button buton1 = findViewById(R.id.btnCadastrar1);

        buton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){

                String email = edtEmail.getText().toString();
                String senha = edtSenha.getText().toString();

                Usuario usuario = new UsuariosDAO(LoginActivity.this).recuperarUsuarioPorEmailESenha(email, senha);

                if(usuario != null) {
                    Intent intent =  new Intent(LoginActivity.this, MainActivity.class);
                    intent.putExtra("usuario", usuario);
                    startActivity(intent);
                }
                else {
                    Toast.makeText(LoginActivity.this, "Verifique seu email e senha.", Toast.LENGTH_SHORT).show();
                }

            }
        });

        buton1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(LoginActivity.this,CadastroDeUsuarioActivity.class);
                startActivity(intent);
            }
        });
    }
}
