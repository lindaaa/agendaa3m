package com.example.agendaescolar;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.example.agendaescolar.modelagem.Evento;
import com.example.agendaescolar.modelagem.Usuario;
import com.example.agendaescolar.modelagem.db.EventosDAO;
import com.example.agendaescolar.modelagem.db.UsuariosDAO;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

public class MainActivity extends AppCompatActivity {

    private ListView listaDeEventos;
    private EventosDAO DAO;

    private AdaptadorDeEventos adaptador;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        DAO = new EventosDAO(this);

//        final Usuario usuario = (Usuario) getIntent().getSerializableExtra("usuario");

        final Usuario usuario = new UsuariosDAO(this).recuperarDadosDoUsuario(1);

        listaDeEventos = findViewById(R.id.lista);
        adaptador = new AdaptadorDeEventos(
                this,
                DAO.recuperarEventosDoUsuario(1)
        );

        listaDeEventos.setAdapter(adaptador);

        registerForContextMenu(listaDeEventos);

        FloatingActionButton fbuton = findViewById(R.id.fab);

        fbuton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent =  new Intent(MainActivity.this, CadastroDeEventoActivity.class);
                intent.putExtra("usuario", usuario);
                startActivity(intent);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();

        adaptador.atualizar(DAO.recuperarEventosDoUsuario(1));
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);

        getMenuInflater().inflate(R.menu.menu_de_contexto, menu);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_de_opcoes, menu);

        return super.onCreateOptionsMenu(menu);
    }

    //Implementar
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == R.id.item_sair) {
            Intent minhaIntent = new Intent(this, LoginActivity.class);
            startActivity(minhaIntent);

            finish();
        }
        else if(item.getItemId() == R.id.item_deletartudo) {
            DAO.deletarTudo();

            this.onResume();
        }

        return super.onOptionsItemSelected(item);
    }

    //Implementar
    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo menuInfo =
                (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();

        Evento eventoSelecionado = (Evento) adaptador.getItem(menuInfo.position);

        if(item.getItemId() == R.id.item_editar) {
            Intent intent = new Intent(this, CadastroDeEventoActivity.class);
            intent.putExtra("Evento", eventoSelecionado);
            startActivity(intent);
        }
        else if(item.getItemId() == R.id.item_deletar) {
            DAO.deletar(eventoSelecionado.getId());

            this.onResume();
        }

        return super.onContextItemSelected(item);
    }
}
