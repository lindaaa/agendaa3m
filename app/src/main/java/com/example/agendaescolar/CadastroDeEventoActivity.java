package com.example.agendaescolar;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.example.agendaescolar.modelagem.Evento;
import com.example.agendaescolar.modelagem.Usuario;
import com.example.agendaescolar.modelagem.db.EventosDAO;

public class CadastroDeEventoActivity extends AppCompatActivity {

    private EditText edtnome;
    private EditText edtdata;
    private EditText edthora_inicial;
    private EditText edthora_final;
    private EditText edtDescricao;
    private RadioGroup rg;

    private Intent minhaIntent;
    private Evento evento;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cadastro_de_evento);

        edtnome = findViewById(R.id.edtNomeDoEvento);
        edtdata = findViewById(R.id.edtData);
        edthora_inicial = findViewById(R.id.edtHoraInicial);
        edthora_final = findViewById(R.id.edtHoraFinal);
        edtDescricao = findViewById(R.id.edtDescricao);

        minhaIntent = getIntent();

        rg = findViewById(R.id.group1);

        if (minhaIntent.hasExtra("Evento")) {
            evento = (Evento) minhaIntent.getSerializableExtra("Evento");

            edtnome.setText(evento.getNome());
            edtdata.setText(evento.getData());
            edthora_inicial.setText(evento.getHoraInicial());
            edthora_final.setText(evento.getHoraFinal());
            edtDescricao.setText(evento.getDescricao());
        }

        Button btnCancelar = findViewById(R.id.btncancelar_evento);
        Button btnCadastrarEvento = findViewById(R.id.btncadastrar_evento);

        btnCancelar.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View view) {
                finish();
            }

        });

        btnCadastrarEvento.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String nome = edtnome.getText().toString();
                String data = edtdata.getText().toString();
                String hora_inicial = edthora_inicial.getText().toString();
                String hora_final = edthora_final.getText().toString();
                String descricao = edtDescricao.getText().toString();
                RadioButton rb = findViewById(rg.getCheckedRadioButtonId());

                Usuario usuario = (Usuario) getIntent().getSerializableExtra("usuario");

                Evento Novoevento = new Evento (
                        0,
                        hora_inicial,
                        hora_final,
                        data,
                        descricao,
                        nome,
                        rb.getText().toString(),
                        usuario.getId()
                );

                EventosDAO DAO = new EventosDAO(CadastroDeEventoActivity.this);

                if (minhaIntent.hasExtra("Evento")){
                    Novoevento.setId(evento.getId());

                    DAO.update(Novoevento);
                }
                else {
                    DAO.insert(Novoevento);
                }

                finish();
            }
        });

    }
}
