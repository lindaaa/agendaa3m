package com.example.agendaescolar.receiver;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;

import com.example.agendaescolar.modelagem.Evento;
import com.example.agendaescolar.notificacoes;

import java.util.Calendar;

public class eventoReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {

        notificacoes not =  new notificacoes();
        Bundle extras = intent.getExtras();
        if (extras != null) {
            String titulo = extras.getString("TITULO");
            String descricao = extras.getString("DESCRICAO");

            not.notificar(context, titulo, descricao);
        }

    }

    public void agendarNotificacao(Evento evento, Context context){
        Calendar calendarioDeAgora = Calendar.getInstance();

        String[] dataCortada = evento.getData().split("/");
        String[] horaCortada = evento.getHoraInicial().split(":");

        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.DAY_OF_MONTH, Integer.parseInt(dataCortada[0]));
        calendar.set(Calendar.MONTH, Integer.parseInt(dataCortada[1]) -1);
        calendar.set(Calendar.YEAR, Integer.parseInt(dataCortada[2]));
        calendar.set(Calendar.HOUR_OF_DAY, Integer.parseInt(horaCortada[0]));
        calendar.set(Calendar.MINUTE, Integer.parseInt(horaCortada[1]));

        if(calendarioDeAgora.before(calendar)) {
            Intent intentAtivarNotificacao = new Intent(context, eventoReceiver.class);
            intentAtivarNotificacao.putExtra("TITULO", evento.getNome());
            intentAtivarNotificacao.putExtra("DESCRICAO", evento.getDescricao());

            PendingIntent p = PendingIntent.getBroadcast(
                    context,
                    1 + evento.getId(),
                    intentAtivarNotificacao,
                    0
            );

            AlarmManager alarmManager = (AlarmManager) context.getSystemService(context.ALARM_SERVICE);

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                alarmManager.setExactAndAllowWhileIdle(
                        AlarmManager.RTC_WAKEUP,
                        calendar.getTimeInMillis(),
                        p
                );
            }
            else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                alarmManager.setExact(
                        AlarmManager.RTC_WAKEUP,
                        calendar.getTimeInMillis(),
                        p
                );
            }
            else {
                alarmManager.set(
                        AlarmManager.RTC_WAKEUP,
                        calendar.getTimeInMillis(),
                        p
                );
            }
        }
    }
}
