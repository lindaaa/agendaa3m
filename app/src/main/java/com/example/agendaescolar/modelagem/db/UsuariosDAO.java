package com.example.agendaescolar.modelagem.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteException;

import com.example.agendaescolar.modelagem.Usuario;

import java.util.ArrayList;

public class UsuariosDAO extends Conexao {

    public UsuariosDAO(Context context) {super(context); }

    public ContentValues obterDados(Usuario usuario){
        ContentValues values = new ContentValues();

        values.put("NOME", usuario.getNome());
        values.put("DATA_DE_NASC", usuario.getDataNascimento());
        values.put("EMAIL", usuario.getEmail());
        values.put("CEP", usuario.getCep());
        values.put("CIDADE", usuario.getCidade());
        values.put("LOGRADOURO", usuario.getLogradouro());
        values.put("SENHA", usuario.getSenha());
        values.put("UF", usuario.getUf());
        values.put("IMG", usuario.getImg());

        return values;
    }

    public void insert(Usuario usuario) {
        ContentValues values = obterDados(usuario);

        getDB().insert("USUARIOS", null, values);
    }

    public void update(Usuario usuario) {
        ContentValues values = obterDados(usuario);

        String argumentos[] = { usuario.getId() + "" };

        getDB().update("USUARIOS", values, "ID=?", argumentos);
    }

    public void deletar(int id){
        String argumentos [] = {id + "" };

        getDB().delete("USUARIOS", "id = ?", argumentos);
    }

    public ArrayList<Usuario> recuperarDadosDoUsuario(int id) {
        ArrayList<Usuario> usuarios = new ArrayList<>();

        try {
            String argumentos[] = { id + "" };

            Cursor cursor = getDB().rawQuery(
                    "SELECT * FROM USUARIOS WHERE ID=?",
                    argumentos
            );

            while(cursor.moveToNext()) {
                usuarios.add(new Usuario(
                        cursor.getInt(cursor.getColumnIndex("ID")),
                        cursor.getString(cursor.getColumnIndex("NAME")),
                        cursor.getString(cursor.getColumnIndex("DATA_DE_NASC")),
                        cursor.getString(cursor.getColumnIndex("EMAIL")),
                        cursor.getString(cursor.getColumnIndex("CEP")),
                        cursor.getString(cursor.getColumnIndex("CIDADE")),
                        cursor.getString(cursor.getColumnIndex("LOGRADOURO")),
                        cursor.getString(cursor.getColumnIndex("SENHA")),
                        cursor.getString(cursor.getColumnIndex("UF")),
                        cursor.getString(cursor.getColumnIndex("IMG"))
                ));
            }
        }
        catch(SQLiteException e) { }

        return usuarios;
    }


}
