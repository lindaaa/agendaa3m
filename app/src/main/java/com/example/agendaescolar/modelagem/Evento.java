package com.example.agendaescolar.modelagem;

import java.io.Serializable;

public class Evento implements Serializable {

    private int id;
    private String horaInicial;
    private String horaFinal;
    private String data;
    private String descricao;
    private String nome;
    private String tipo;
    private int idDoUsuario;

    public Evento(int id, String horaInicial, String horaFinal, String data, String descricao, String nome, String tipo, int idDoUsuario) {
        this.id = id;
        this.horaInicial = horaInicial;
        this.horaFinal = horaFinal;
        this.data = data;
        this.descricao = descricao;
        this.nome = nome;
        this.tipo = tipo;
        this.idDoUsuario = idDoUsuario;
    }

    public int getIdDoUsuario() {
        return idDoUsuario;
    }

    public void setIdDoUsuario(int idDoUsuario) {
        this.idDoUsuario = idDoUsuario;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getHoraInicial() {
        return horaInicial;
    }

    public void setHoraInicial(String horaInicial) {
        this.horaInicial = horaInicial;
    }

    public String getHoraFinal() {
        return horaFinal;
    }

    public void setHoraFinal(String horaFinal) {
        this.horaFinal = horaFinal;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }
}
