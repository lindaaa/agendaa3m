package com.example.agendaescolar.modelagem.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteException;

import com.example.agendaescolar.modelagem.Evento;

import java.util.ArrayList;

public class EventosDAO extends Conexao {

    public EventosDAO(Context context) {
        super(context);
    }

    public ContentValues obterDados(Evento evento) {
        ContentValues values = new ContentValues();

        values.put("NOME", evento.getNome());
        values.put("HORA_INICIAL", evento.getHoraInicial());
        values.put("HORA_FINAL", evento.getHoraFinal());
        values.put("DATA", evento.getData());
        values.put("TIPO", evento.getTipo());
        values.put("DESCRICAO", evento.getDescricao());
        values.put("ID_DO_USUARIO", evento.getIdDoUsuario());

        return values;
    }

    public void insert(Evento evento) {
        ContentValues values = obterDados(evento);

        getDB().insert("EVENTOS", null, values);
    }

    public void update(Evento evento) {
        ContentValues values = obterDados(evento);

        String argumentos[] = { evento.getId() + "" };

        getDB().update("EVENTOS", values, "ID=?", argumentos);
    }

    public void deletar(int id) {
        String argumentos[] = { id + "" };

        getDB().delete("EVENTOS", "ID=?", argumentos);
    }

    public ArrayList<Evento> recuperarEventosDoUsuario(int idDoUsuario) {
        ArrayList<Evento> eventos = new ArrayList<>();

        try {
            String argumentos[] = { idDoUsuario + "" };

            Cursor cursor = getDB().rawQuery(
                    "SELECT * FROM EVENTOS WHERE ID_DO_USUARIO=?",
                    argumentos
            );

            while(cursor.moveToNext()) {
                eventos.add(new Evento(
                        cursor.getInt(cursor.getColumnIndex("ID")),
                        cursor.getString(cursor.getColumnIndex("HORA_INICIAL")),
                        cursor.getString(cursor.getColumnIndex("HORA_FINAL")),
                        cursor.getString(cursor.getColumnIndex("DATA")),
                        cursor.getString(cursor.getColumnIndex("DESCRICAO")),
                        cursor.getString(cursor.getColumnIndex("NOME")),
                        cursor.getString(cursor.getColumnIndex("TIPO")),
                        cursor.getInt(cursor.getColumnIndex("ID_DO_USUARIO"))

                ));
            }
        }
        catch(SQLiteException e) { }

        return eventos;
    }

    public void deletarTudo() {
        getDB().execSQL("DELETE FROM EVENTOS;");
    }
}
