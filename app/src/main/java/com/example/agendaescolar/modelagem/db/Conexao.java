package com.example.agendaescolar.modelagem.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class Conexao extends SQLiteOpenHelper {

    public Conexao(Context context) {
        super(context, "a4m", null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE EVENTOS(ID INTEGER PRIMARY KEY AUTOINCREMENT, NOME VARCHAR(80), HORA_INICIAL VARCHAR(5), HORA_FINAL VARCHAR(5), DATA VARCHAR(11), TIPO VARCHAR(20), DESCRICAO TEXT, ID_DO_USUARIO INTEGER, FOREIGN KEY (ID_DO_USUARIO) REFERENCES USUARIOS(ID));");
        db.execSQL("CREATE TABLE USUARIOS(ID INTEGER PRIMARY KEY AUTOINCREMENT, NOME VARCHAR(60), EMAIL VARCHAR(50), SENHA VARCHAR(50), DATA_DE_NASC VARCHAR(11), CEP VARCHAR(9), LOGRADOURO VARCHAR(9), CIDADE VARCHAR(15), UF VARCHAR(3));");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE EVENTOS;");
        db.execSQL("DROP TABLE USUARIOS;");
        this.onCreate(db);
    }

    public SQLiteDatabase getDB() {
        return getWritableDatabase();
    }
}
